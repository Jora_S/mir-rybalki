import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 25.04.18
 * Time: 9:52
 * To change this template use File | Settings | File Templates.
 */
public class BaseTestsMir {
    WebDriver driver = initChromedriver();
    @Test
    public void Test1(){
        driver.get("https://mir-rybalki.com/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement setki = driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/ul/li[1]/div/ul/li[5]/a"));
        setki.click();
        WebElement rakolovki = driver.findElement(By.linkText("Ятеря,раколовки"));
        rakolovki.click();
        WebElement razmer = driver.findElement(By.id("link_to_product_689382518"));
        razmer.click();
        WebElement price = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/p/span[1]"));
        String priceText = price.getText();
        assertEquals("208,70", priceText);
        System.out.println("208,70");
        WebElement kupit = driver.findElement(By.linkText("Купить"));
        kupit.click();
        WebElement sravnit = driver.findElement(By.className("x-shc-total__price"));
        String sravnitText = sravnit.getText();
        assertEquals("208,70 грн.", sravnitText);
        System.out.println("208,70 - покупай!");
        WebElement udalit = driver.findElement(By.className("x-shc-item__control-icon"));
        udalit.click();
        System.out.println("передумал!");
        WebElement nachat = driver.findElement(By.className("x-empty-results__link"));
        nachat.click();
        driver.quit();

    }
    @Test
    public void Test2() {
        driver.get("https://mir-rybalki.com/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement palatki = driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/ul/li[1]/div/ul/li[20]/a"));
        palatki.click();
        WebElement palatka = driver.findElement(By.id("link_to_product_12891194"));
        palatka.click();
        WebElement price = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/p/span[1]"));
        String priceText = price.getText();
        assertEquals("661,50", priceText);
        System.out.println("661,50 - normal!");
        WebElement kupil = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/a"));
        kupil.click();
        WebElement otkaz = driver.findElement(By.className("x-shc-item__control-icon"));
        otkaz.click();
        System.out.println("Передумал снова...");
        WebElement prodolgit = driver.findElement(By.className("x-button__text"));
        prodolgit.click();
        WebElement proverka = driver.findElement(By.className("b-shopping-button__text"));
        proverka.click();
        WebElement vernulsya = driver.findElement(By.xpath("/html/body/div[17]/div/div/div/div/div[2]/div/a"));
        System.out.println("В корзине нет товаров!!!");
        driver.quit();

    }
    @Test
    public void Test3(){
        driver.get("https://mir-rybalki.com/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement korzina = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/span[2]"));
        korzina.click();
    }

    public static WebDriver initChromedriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }

}
